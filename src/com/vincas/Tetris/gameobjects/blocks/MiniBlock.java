package com.vincas.Tetris.gameobjects.blocks;

import org.newdawn.slick.Color;

public class MiniBlock extends OBlock {
	private boolean resized = false;
	
	public MiniBlock(Color color) {
		super(color);
	}

	@Override
	protected void initializeBlock() {
		if (!resized)
			super.initializeBlock();
		else {
			sheets = new Square[4][4][4];
			sheets[0][0][0] = new Square(getColor());
		}
	}

	@Override
	public void recolor() {
		if (resized) {
			super.setColor(Color.lightGray);
			initializeBlock();
		}
		else
			super.recolor();
		
	}

	public void resize() {
		resized = true;
		initializeBlock();
	}
	
	@Override
	public String toString() {
		return String.format("MiniBlock [%s]", super.getColor());
	}
}
