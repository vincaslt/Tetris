package com.vincas.Tetris.gui;

public interface Clickable extends Drawable {
	public void clicked(int button, int x, int y);
}
