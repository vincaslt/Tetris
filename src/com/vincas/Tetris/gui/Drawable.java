package com.vincas.Tetris.gui;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Drawable {
	public void draw(GameContainer c, Graphics g);
}
