package com.vincas.Tetris.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class ExitButton extends MenuButton {
    public ExitButton(GameContainer c, int x, int y, String text) {
        super(c, x, y, text);
    }
    
    @Override
    protected void onClick() {
        container.exit();
    }

    @Override
    public void draw(GameContainer c, Graphics g) {
        area.render(c, g);
        g.setColor(Color.blue);
        g.drawString(text, x + 55, y + 10);
    }
}
