package com.vincas.Tetris.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.MouseOverArea;

public abstract class MenuButton implements Clickable {
    protected int x, y;
	protected MouseOverArea area;
    protected String text;
    protected GameContainer container;
	
	public MenuButton(GameContainer c, int x, int y, String text) {
		area = new MouseOverArea(c, null, new Rectangle(x, y, 150, 40));
		area.setNormalColor(Color.orange);
		area.setMouseOverColor(Color.orange);
		area.setMouseDownColor(Color.yellow);
        this.x = x;
        this.y = y;
        this.text = text;
        container = c;
	}

	protected abstract void onClick();

	@Override
	public final void clicked(int button, int x, int y) {
		area.mousePressed(button, x, y);
		if (area.isMouseOver()) {
			onClick();
		}
	}
}
