package com.vincas.Tetris.gui;

import com.vincas.Tetris.Tetris;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class StartButton extends MenuButton {
	private StateBasedGame game;
	
	public StartButton(StateBasedGame game, int x, int y, String text) {
		super(game.getContainer(), x, y, text);
		this.game = game;
	}

	@Override
	protected void onClick() {
		this.game.enterState(Tetris.STATE_GAMEPLAY, new FadeOutTransition(), new FadeInTransition());
	}

	@Override
	public void draw(GameContainer c, Graphics g) {
		area.render(c, g);
		g.setColor(Color.blue);
		g.drawString(text, x + 48, y + 10);
	}
}
