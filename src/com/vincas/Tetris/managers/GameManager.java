package com.vincas.Tetris.managers;

import com.vincas.Tetris.Tetris;
import com.vincas.Tetris.gameobjects.GameField;
import com.vincas.Tetris.gameobjects.blocks.Block;
import com.vincas.Tetris.gameobjects.blocks.IBlock;
import com.vincas.Tetris.utils.*;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import java.util.Random;

public class GameManager {
	//private static final int MAX_LEVEL = 9;
	
	private Timer timer;
	private int level; //TODO increasing difficulty
	private GameField field;
	private StateBasedGame game;
	private boolean isActive;
	private ScoreManager scoreManager;
    private BlockFactory blockFactory;
    private Block nextBlock = null;
	
	private int totalRowsRemoved = 0;

	public GameManager(StateBasedGame game, GameField field) {
		this(game, 1, field);
	}

	public GameManager(StateBasedGame game, int level, GameField field) {
		timer = new Timer(1000 - level * 100);
		this.level = level;
		this.field = field;
		this.game = game;
		isActive = true;
		scoreManager = new ScoreManager();
        blockFactory = new BlockFactory();
	}
	
	public void speedUp() {
		triggerGravity();
		timer.restartTimer(100);
	}
	
	public void normalizeSpeed() {
		timer.restartTimer(1000 - level * 100);
	}

	public void update(int delta, GameContainer container, StateBasedGame game) throws WindowInterruptionException {
		if (!isActive) return;
        
        if (!container.hasFocus()) {
            throw new WindowInterruptionException(game.getTitle());
        }
        
        if (delta > 200) {
            throw new WindowFreezeException(delta);
        }
		
		timer.update(delta);
		if (timer.isTimeComplete()) {
			triggerGravity();
			timer.resetTime();
		}
	}

	public boolean triggerGravity() {
		try {
			if(field.getActiveBlock().translate(field, 0, 1)) {
				return true;
			}
			else {
				spawnNewBlock();
				checkRows();
				return false;
			}
		} catch(GameOverException e) {
			handleGameOver();
			return false;
		}
	}
	
	private void checkRows() {
		boolean trigger;
		int rowsRemoved = 0;
		for (int r = 0; r < field.getHeight(); r++) {
			trigger = true;
			for (int c = 0; c < field.getWidth(); c++) {
				if (field.getSquares()[r][c] == null) {
					trigger = false;
					break;
				}
			}
			if (trigger) {
				removeRow(r);
				rowsRemoved++;
			}
		}
		totalRowsRemoved += rowsRemoved;
		scoreManager.addScore(rowsRemoved, level);
		tryLevelUp();
	}
    
    public Block getNextBlock() {
        return nextBlock;
    }
	
	private void tryLevelUp() {
		if ((totalRowsRemoved >= 5 && level == 1) ||
			(totalRowsRemoved >= 15 && level == 2) ||
			(totalRowsRemoved >= 25 && level == 3) ||
			(totalRowsRemoved >= 35 && level == 4) ||
			(totalRowsRemoved >= 45 && level == 5) ||
			(totalRowsRemoved >= 55 && level == 6) ||
			(totalRowsRemoved >= 65 && level == 7) ||
			(totalRowsRemoved >= 80 && level == 8)) {
			level += 1;
			normalizeSpeed();
		}
	}

	private void removeRow(int row) {
		if (row < 0) return;
		
		for (int c = 0; c < field.getWidth(); c++) {
			field.getSquares()[row][c] = null;
		}
		pullDown(row);
	}
	
	private void pullDown(int row) {
		if (row - 1 < 0) return;
		
		for (int c = 0; c < field.getWidth(); c++) {
			if (field.getSquares()[row - 1][c] != null) {
				field.getSquares()[row][c] = field.getSquares()[row - 1][c];
			}
		}

		removeRow(row - 1);
	}
    
    public void generateNextBlock() {
        int blockType = (new Random()).nextInt(8);
        nextBlock = blockFactory.getBlock(BlockFactory.BlockType.values()[blockType]);
    }

	public void spawnNewBlock() {
        if (nextBlock == null)
            generateNextBlock();
        
        try {
            Block block = (Block) nextBlock.clone();
            field.addActiveBlock(3, -2, block);
            timer.resetTime();
            generateNextBlock();
            triggerGravity();

            if (!(block instanceof IBlock))
                triggerGravity();

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
	}

	private void handleGameOver() {
        HighScoreManager.getInstance().addScore(scoreManager.getScore());
        HighScoreManager.getInstance().saveHighScores();
		game.enterState(Tetris.STATE_GAMEOVER, new FadeOutTransition(), new FadeInTransition());
		isActive = false;
	}
	
	public ScoreManager getScores() {
		return scoreManager;
	}
}
