package com.vincas.Tetris.managers;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class HighScoreManager {
    public static final String FILE_NAME = "highscores.dat";
    ArrayList<Integer> highScores = new ArrayList<Integer>();
    
    public static HighScoreManager instance = new HighScoreManager();

    private HighScoreManager() {
        loadHighscores();
    }

    private void loadHighscores() {
        try {
            ObjectInputStream oos = new ObjectInputStream(new FileInputStream(FILE_NAME));
            highScores = (ArrayList<Integer>) oos.readObject();
            if (highScores == null)
                highScores = new ArrayList<Integer>();
            oos.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void saveHighScores() {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FILE_NAME));
            oos.writeObject(highScores);
            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void addScore(int score) {
        highScores.add(score);
        Collections.sort(highScores, Collections.reverseOrder());
        if (highScores.size() > 10) {
            highScores.remove(highScores.size() - 1);
        }
    }
    
    public ArrayList<Integer> getHighscores() {
        return this.highScores;
    }

    public static HighScoreManager getInstance() {
        return instance;
    }
}
