package com.vincas.Tetris.states;

import com.vincas.Tetris.Tetris;
import com.vincas.Tetris.gui.StartButton;
import com.vincas.Tetris.managers.HighScoreManager;
import org.newdawn.slick.*;
import org.newdawn.slick.opengl.TextureImpl;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class GameOverState extends BasicGameState {

    Image gameOverImage;
    StartButton restartButton;

	@Override
	public int getID() {
		return Tetris.STATE_GAMEOVER;
	}

	@Override
	public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        gameOverImage = (new Image("assets/game_over.png")).getScaledCopy(0.3f);
        restartButton = new StartButton(stateBasedGame, Tetris.SCREEN_WIDTH / 2 - 70, Tetris.SCREEN_HEIGHT / 2 + 200, "Restart");
	}

	@Override
	public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
		graphics.drawImage(gameOverImage, Tetris.SCREEN_WIDTH / 2 - gameOverImage.getWidth() / 2,
                           80 - gameOverImage.getHeight() / 2);
        graphics.setColor(Color.blue);
        graphics.fillRect(Tetris.SCREEN_WIDTH / 2 - 100, Tetris.SCREEN_HEIGHT / 2 - 140, 200, 300);
        
        graphics.setColor(Color.yellow);
        graphics.drawString("Highscores: ", Tetris.SCREEN_WIDTH / 2 - 40, Tetris.SCREEN_HEIGHT / 2 - 120);
        int i = 0;
        for (int score : HighScoreManager.getInstance().getHighscores()) {
            i++;
            graphics.drawString("#" + i + ". " + score, Tetris.SCREEN_WIDTH / 2 - 25, Tetris.SCREEN_HEIGHT / 2 - 90 + (i * 20));
        }

        restartButton.draw(gameContainer, graphics);
		TextureImpl.bindNone();
	}

	@Override
	public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {

	}

    @Override
    public void mousePressed(int button, int x, int y) {
        restartButton.clicked(button, x, y);
    }
}
