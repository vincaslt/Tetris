package com.vincas.Tetris.states;

import com.vincas.Tetris.Tetris;
import com.vincas.Tetris.gui.ExitButton;
import com.vincas.Tetris.gui.StartButton;
import org.newdawn.slick.*;
import org.newdawn.slick.opengl.TextureImpl;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class MainMenuState extends BasicGameState {
	StateBasedGame game;
	StartButton startButton;
    Image tetrisImage;
    ExitButton exitButton;
	
	@Override
	public int getID() {
		return Tetris.STATE_MAINMENU;
	}

    @Override
    public void enter(GameContainer container, StateBasedGame game) throws SlickException {

    }

    @Override
	public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
		this.game = stateBasedGame;
        tetrisImage = (new Image("assets/tetris.jpg")).getScaledCopy(0.5f);
		startButton = new StartButton(stateBasedGame, Tetris.SCREEN_WIDTH / 2 - 70, tetrisImage.getHeight() + 50, "Start");
	    exitButton = new ExitButton(gameContainer, Tetris.SCREEN_WIDTH / 2 - 70, tetrisImage.getHeight() + 110, "Exit");
    }

	@Override
	public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
		graphics.setColor(new Color(0x000E, 0x0096, 0x00D4));
        graphics.fillRect(0, 0, Tetris.SCREEN_WIDTH, Tetris.SCREEN_HEIGHT);
        graphics.setColor(new Color(0x0024, 0x00B0, 0x00E1));
        graphics.fillRect( Tetris.SCREEN_WIDTH / 2 - tetrisImage.getWidth() / 2,
                tetrisImage.getHeight(), tetrisImage.getWidth(), Tetris.SCREEN_HEIGHT - tetrisImage.getHeight());
        graphics.drawImage(tetrisImage, Tetris.SCREEN_WIDTH / 2 - tetrisImage.getWidth() / 2, 0);
        startButton.draw(gameContainer, graphics);
        exitButton.draw(gameContainer, graphics);
		TextureImpl.bindNone();
	}

	@Override
	public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {

	}

	@Override
	public void mousePressed(int button, int x, int y) {
		exitButton.clicked(button, x, y);
        startButton.clicked(button, x, y);
	}
}
