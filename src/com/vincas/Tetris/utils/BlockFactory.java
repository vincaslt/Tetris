package com.vincas.Tetris.utils;

import com.vincas.Tetris.gameobjects.blocks.*;
import org.newdawn.slick.Color;

public class BlockFactory {
    public enum BlockType {
        IBLOCK, JBLOCK, TBLOCK, OBLOCK, LBLOCK, ZBLOCK, SBLOCK, MINIBLOCK
    }
    
    public Block getBlock(BlockType type) {
        if (type == BlockType.IBLOCK) {
            return new IBlock(Color.cyan);
        }
        else if (type == BlockType.JBLOCK) {
            return new JBlock(Color.blue);
        }
        else if (type == BlockType.LBLOCK) {
            return new LBlock(Color.orange);
        }
        else if (type == BlockType.OBLOCK) {
            return new OBlock(Color.yellow);
        }
        else if (type == BlockType.SBLOCK) {
            return new SBlock(Color.green);
        }
        else if (type == BlockType.TBLOCK) {
            return new TBlock(Color.magenta);
        }
        else if (type == BlockType.ZBLOCK) {
            return new ZBlock(Color.red);
        }
        else if (type == BlockType.MINIBLOCK) {
            return new MiniBlock(Color.white);
        }
        return null;
    }
}
