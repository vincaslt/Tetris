package com.vincas.Tetris.utils;

public class WindowFreezeException extends WindowInterruptionException {
    private int delay;
    
    public WindowFreezeException(int delay) {
        super("Tetris");
        this.delay = delay;
    }

    @Override
    public String getLocalizedMessage() {
        return "Langas sustingdytas " + delay + " milisekundėms!";
    }
}
