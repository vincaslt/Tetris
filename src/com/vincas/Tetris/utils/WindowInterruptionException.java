package com.vincas.Tetris.utils;

public class WindowInterruptionException extends Exception {
    protected String windowName;
    
    public WindowInterruptionException(String windowName) {
        super("A window was interrupted!");
        this.windowName = windowName;
    }

    @Override
    public void printStackTrace() {
        super.printStackTrace();

        System.out.println("Cause is in window " + windowName);
    }
}

